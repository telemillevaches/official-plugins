# PeerTube transcoding custom quality

This plugin creates a transcoding profile in which admins can decide the quality of the transcoding process.
Here are the CRF value used by ffmpeg for each label :

- 'Low' -> CRF = 35
- 'Medium' -> CRF = 32
- 'Good' -> CRF = 29
- 'Very good' -> CRF = 26
- 'Excellent (Peertube default uncapped)' -> CRF = 23
- 'Perfect' -> CRF = 20
- 'Insane' -> CRF = 17

**Increasing quality will result in bigger video sizes**.

NB : Peertube default encoding settings cap the bit rate of videos. Image quality decreases sensibly in scenes with a lot of moving details, like a tracking shot in a landscape. This plugin removes the cap, resulting in constant quality, high bit rates on such scenes. Depending on your videos, the "Excellent (Peertube default uncapped)" setting might yield much bigger size and higher quality than Peertube default.

--------------------

Once installed and your value set in the plugin parameters, you have to choose "**custom-quality**" option in Administration/Configuration/VOD Transcoding/Transcoding profile.

--------------------

Explanations (source : https://trac.ffmpeg.org/wiki/Encode/H.264)

CRF means _Constant Rate Factor_.
This method allows the encoder to attempt to achieve a certain output quality for the whole file when output file size is of less importance. This provides maximum compression efficiency with a single pass. By adjusting the so-called quantizer for each frame, it gets the bitrate it needs to keep the requested quality level. The downside is that you can't tell it to get a specific filesize or not go over a specific size or bitrate, which means that this method is not recommended for encoding videos for streaming.
The range of the CRF scale is 0–51, where 0 is lossless, 23 is the default (for ffmpeg), and 51 is worst quality possible. A lower value generally leads to higher quality, and a subjectively sane range is 17–28. Consider 17 or 18 to be visually lossless or nearly so; it should look the same or nearly the same as the input but it isn't technically lossless.
The range is exponential, so increasing the CRF value +6 results in roughly half the bitrate / file size, while -6 leads to roughly twice the bitrate.
Choose the highest CRF value that still provides an acceptable quality. If the output looks good, then try a higher value. If it looks bad, choose a lower value.

Note: The 0–51 CRF quantizer scale mentioned on this page only applies to 8-bit x264. For 10-bit support, refer to the documentation on ffmpeg.org
